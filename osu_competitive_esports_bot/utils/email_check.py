async def valid_email(ctx):
    if len(ctx.message.content.split(' ')) > 1:
        email = ctx.message.content.split(' ')[1]
        return ('@' in email and (email.endswith('@osu.edu') or email.endswith('@buckeyemail.osu.edu')))
    else:
        return False


async def valid_code(ctx):
    import re

    code_regex = re.compile("^[0-9]*$")

    if len(ctx.message.content.split(' ')) > 1:
        code = ctx.message.content.split(' ')[1]
        return code_regex.match(code)
    else:
        return False
