async def is_user_osu(ctx):
    from osu_competitive_esports_bot.main import DISCORD_VERIFICATION_ROLES
    print('osu check')
    for guild in ctx.bot.guilds:
        member = guild.get_member(ctx.author.id)
        if member is not None:
            for role in DISCORD_VERIFICATION_ROLES:
                for user_role in member.roles:
                    if role == user_role.name:
                        return True

    return False
