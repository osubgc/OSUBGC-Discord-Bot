from osu_competitive_esports_bot.utils.role_list import send_allowed_role_list
from osu_competitive_esports_bot.utils.role_list import send_full_role_list
from osu_competitive_esports_bot.utils.admin_check import admin_check
from osu_competitive_esports_bot.main import DISCORD_BOT
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command(name='game-notify-role')
@commands.check(admin_check)
async def game_notify_role(ctx, state, *, roles):
    from osu_competitive_esports_bot.main import delete_message, settings
    import json

    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !game-notify-role command.")

    if state == 'add':
        if 'game_notification_roles' not in settings['discord']:
            settings['discord']['game_notification_roles'] = []
        for new_role in roles.split(','):
            added = False
            found = False

            new_role = new_role.strip()

            if new_role not in settings['discord']['game_notification_roles']:
                for guild in ctx.bot.guilds:
                    for role in guild.roles:
                        if new_role.strip().lower() == role.name.lower():
                            settings['discord']['game_notification_roles'].append(role.name.lower())
                            added = True
                            found = True
            else:
                added = True

            with open('settings.json', 'w') as f:
                json.dump(settings, f, ensure_ascii=False, indent=4, sort_keys=True)

            if added:
                await ctx.message.author.send(
                    ctx.message.author.mention + ' - `' + new_role +
                    '` has been successfully added to '
                    'allow users to receive notifications '
                    'for.')
            elif not found:
                send_role_list = True
                await ctx.message.author.send(
                    ctx.message.author.mention + ' - Sorry, but `' + new_role +
                    '` does not seem to exist.\n')
    elif state == 'remove':
        if 'game_notification_roles' in settings['discord']:
            for remove_role in roles.split(','):
                remove_role = remove_role.strip().lower()
                if remove_role in settings['discord']['game_notification_roles']:
                    settings['discord']['game_notification_roles'].remove(remove_role)

                    with open('settings.json', 'w') as f:
                        json.dump(settings, f, ensure_ascii=False, indent=4, sort_keys=True)

                    await ctx.send(
                        ctx.message.author.mention + ' - `' + remove_role +
                        '` was successfully removed.')
                else:
                    await ctx.message.author.send(
                        ctx.message.author.mention + ' - Sorry, but `' + remove_role +
                        '` was not already '
                        'in the allowed list.\n')
                    send_role_list = True

    await delete_message(ctx)
    logger.info("!game-notify-role command complete.")


@game_notify_role.error
async def game_notify_role_error(ctx, error):
    logger.warning(error)

    if isinstance(error, commands.CheckFailure):
        await ctx.message.author.send('You do not have permission to use this command.')
    elif isinstance(error, commands.CommandError):
        await ctx.message.author.send('Something happened and I was unable to complete this request correctly.  Please see `!help`...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)

